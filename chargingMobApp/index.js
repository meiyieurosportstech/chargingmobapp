import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import t from 'tcomb-form-native';
import { Button } from 'react-native-elements'
// Assets
import scorpImage from './assets/images/scorpio.jpg'
import StatusBar from 'react-native'

const Form = t.form.Form;

const User = t.struct({
  email: t.String,
  password: t.String,
  rememberMe: t.Boolean,
  terms: t.Boolean
});

export default class App extends React.Component {
  handleSubmit = () => {
    // do the things  
  }
  render() {
    return (
      <View style={styles.container}>
        <Form type={User} />
        <Button
          icon={{
            size: 15,
            color: "white"
          }}
          title="Button with icon object"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textStyle: {
    textAlign: 'center',
    fontSize: 25,
    fontFamily: "Courier New",
    color: "#555555",
    marginBottom: 5
  }
});
